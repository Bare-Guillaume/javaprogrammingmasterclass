package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        boolean nameStatus = false;
        boolean dateStatus = false;
        do {
            Scanner scanner = new Scanner(System.in);

            System.out.println("Enter your year of birth:  ");

            boolean hasNextInt = scanner.hasNextInt();
            if (hasNextInt){
                int yearOfBirth = scanner.nextInt();
                scanner.nextLine();
                System.out.println("Enter your name: ");
                String name = scanner.nextLine();// handle the enter type key
                nameStatus = true;
                int age = 2018 - yearOfBirth;

                if(age > 0 && age <= 100){
                    dateStatus = true;
                    System.out.println("Your name is " + name + " and your age is " + age);
                } else {
                    System.out.println("Invalid year of birth");
                }

            } else {
                System.out.println("Unable to parse birthdate");
            }
            scanner.close();
        }while (!nameStatus || !dateStatus);

    }
}
