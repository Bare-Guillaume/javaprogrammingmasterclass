package com.company;

public class Fiat extends Car {
    public Fiat(String name, int cylinder) {
        super(name, cylinder);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String startEngine() {
        return getClass().getSimpleName() + " " + getName() + " -> startEngine()";
    }

    @Override
    public String brake() {
        return getClass().getSimpleName() + " " + getName() + " -> brake()";
    }

    @Override
    public String accelerate() {
        return  getClass().getSimpleName() + " " + getName() + " -> accelerate()";
    }
}
