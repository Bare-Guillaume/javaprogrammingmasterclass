package com.company;

public class Mitsubishi extends Car{
    public Mitsubishi(String name, int cylinder) {
        super(name,cylinder);
    }

    @Override
    public String startEngine() {
        return "Mitsubishi -> startEngine()";
    }

    @Override
    public String brake() {
        return "Mitsubishi -> brake()";
    }

    @Override
    public String accelerate() {
        return "Mitsubishi -> accelerate()";
    }
}
