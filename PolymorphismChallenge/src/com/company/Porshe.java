package com.company;

public class Porshe extends Car {
    public Porshe(String name, int cylinder) {
        super(name, cylinder);
    }

    @Override
    public String startEngine() {
        return "Porshe -> startEngine()";
    }

    @Override
    public String brake() {
        return "Porshe -> brake()";
    }

    @Override
    public String accelerate() {
        return "Porshe -> accelerate()";
    }
}
