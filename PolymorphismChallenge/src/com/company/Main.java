package com.company;

public class Main {

    public static void main(String[] args) {
		Car car = new Car("Base car", 8);
		System.out.println(car.startEngine());
		System.out.println(car.accelerate());
		System.out.println(car.brake());

		Mitsubishi mitsubishi = new Mitsubishi("Lancer", 4);
		System.out.println(mitsubishi.startEngine());
		System.out.println(mitsubishi.accelerate());
		System.out.println(mitsubishi.brake());

		Porshe porshe = new Porshe("Carrera", 4);
		System.out.println(porshe.startEngine());
		System.out.println(porshe.accelerate());
		System.out.println(porshe.brake());

		Fiat fiat = new Fiat("Punto", 4);
		System.out.println(fiat.startEngine());
		System.out.println(fiat.accelerate());
		System.out.println(fiat.brake());

		Fiat fiat2 = new Fiat("Panda", 4);
		System.out.println(fiat2.startEngine());
		System.out.println(fiat2.accelerate());
		System.out.println(fiat2.brake());

    }
}
