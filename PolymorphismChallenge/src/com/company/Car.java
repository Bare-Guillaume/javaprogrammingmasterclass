package com.company;

public class Car {
    private String name;
    private Boolean engine;
    private int cylinder;
    private int wheels;

    public Car(String name,int cylinder) {
        this.name = name;
        this.engine = true;
        this.cylinder = cylinder;
        this.wheels = 4;
    }

    public String getName() {
        return name;
    }

    public int getCylinder() {
        return cylinder;
    }

    public String startEngine() {
        return "Car -> startEngine()";
    }

    public String brake() {
        return "Car -> brake()";
    }

    public String accelerate() {
        return "Car -> Accelerating()";
    }
}
