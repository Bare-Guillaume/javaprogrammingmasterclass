package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println(NumberPalindrome.isPalindrome(12321));
        System.out.println(NumberPalindrome.isPalindrome(123));
        System.out.println(NumberPalindrome.isPalindrome(12621));
    }
}
