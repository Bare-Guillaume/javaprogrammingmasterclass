package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println(getDurationString(45, 30));
        System.out.println(getDurationString(360));
        System.out.println(getDurationString(75, 45));
        System.out.println(getDurationString(-41));
    }

    private static final String INVALID_FINAL_MESSAGE = "Invalid Value";

    public static String getDurationString(long minutes, long seconds){
        if((minutes >= 0) && (seconds >= 0 && seconds <= 59)){

            long hours = minutes / 60;
            long remainingMinutes = minutes % 60;

            String hourString = hours + "h";
            if (hours < 10){
                hourString = "0"+ hourString;
            }
            String minuteString = minutes + "m";
            if (remainingMinutes < 10){
                minuteString = "0"+ minuteString;
            }
            String secondString = seconds + "m";
            if (seconds < 10){
                secondString = "0"+ secondString;
            }
            return hourString + "  " + minuteString + "  " + secondString;


        } else {
            return INVALID_FINAL_MESSAGE;
        }
    }

    public static String getDurationString(int seconds){
        if( seconds > 0 ){
            int minutes = seconds / 60;
            int remainingSeconds = seconds % 60;
            return getDurationString(minutes, remainingSeconds);
        }

            return INVALID_FINAL_MESSAGE;

    }
}
