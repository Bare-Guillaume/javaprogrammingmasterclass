package com.company;

public class Main {

    public static void main(String[] args) {
	    BankAccount firstAccount = new BankAccount("12345", 95, "Guillaume",
				"bare.guillaume@gmail.com", "0486627990");

		System.out.println(firstAccount.getAccountNumber());
		System.out.println(firstAccount.getBalance());
		System.out.println(firstAccount.getCustomerName());
		System.out.println(firstAccount.getEmail());
		System.out.println(firstAccount.getPhoneNumber());

		firstAccount.deposit(5);

		System.out.println(firstAccount.getBalance());

		firstAccount.withdrawal(40);

		System.out.println(firstAccount.getBalance());

		BankAccount secondAccount = new BankAccount();
		System.out.println(secondAccount.getAccountNumber());
		System.out.println(secondAccount.getBalance());
		System.out.println(secondAccount.getCustomerName());
		System.out.println(secondAccount.getEmail());
		System.out.println(secondAccount.getPhoneNumber());

		VipCustomer firstVip = new VipCustomer();

		VipCustomer secondVip = new VipCustomer("Guillaume",500, "bare.guillaume@gmail.com");

		VipCustomer thirdVip = new VipCustomer("Tatiana", "tatiana@hotmail.com");

		System.out.println(firstVip.getName());
		System.out.println(firstVip.getEmail());
		System.out.println(firstVip.getCreditLimit());

		System.out.println(secondVip.getName());
		System.out.println(secondVip.getEmail());
		System.out.println(secondVip.getCreditLimit());

		System.out.println(thirdVip.getName());
		System.out.println(thirdVip.getEmail());
		System.out.println(thirdVip.getCreditLimit());
    }
}
