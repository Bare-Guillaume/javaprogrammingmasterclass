package com.company;

public class BankAccount {

    public String accountNumber;
    public double balance;
    public String customerName;
    public String email;
    public String phoneNumber;

    public BankAccount(String accountNumber, double balance, String customerName, String email, String phoneNumber) {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.customerName = customerName;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }
    //default constructor
    public BankAccount(){
        this("666", 0, "John Doe", "noEmail", "noPhone");
        System.out.println("Empty constructor called");
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void deposit(double amount){
        this.balance += amount;
    }

    public void withdrawal(int amount){

        if(this.balance - amount < 0){
            System.out.println("not enough funds");
        } else {
            this.balance -= amount;
            System.out.println("your current balance is now " + getBalance());
        }
    }
}
