public class BarkingDog {
    public static boolean shouldWakeUp(boolean barking, int hoursOfDay) {
        if (hoursOfDay < 0 || hoursOfDay > 23){
            return false;
        } else if ( (barking == true && hoursOfDay < 8) || (barking == true && hoursOfDay > 22) ) {
            return true;
        }
        return false;
    }
}
