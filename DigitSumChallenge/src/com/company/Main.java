package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println(sumDigits(125));
    }

    public static int sumDigits(int number){
        int sum = 0;
        if(number < 10) {
            return -1;
        }
        while( number > 0){
            //extract least significant digit.
            int digit = number % 10;
            sum += digit;
            // Drop least significant digit.
            number /=10;
        }
        return sum;

    }
}
