public class DecimalComparator {
    public static boolean areEqualByThreeDecimalPlaces(double firstParam, double secondParam){
        firstParam = (int) (firstParam * 1000);
        secondParam = (int) (secondParam * 1000);

        return firstParam == secondParam;
    }
}
