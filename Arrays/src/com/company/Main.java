package com.company;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        //1.array declaration + call getIntegers() function with param 5
	    int[] myIntegers = getIntegers(5);
        //7. the array myIntegers got all values from array values and output all of them from the loop
	    for (int i = 0; i< myIntegers.length; i++){
            System.out.println("Element " + i + ", typed value was " + myIntegers[i]);
        }
	    //8. Call the method getAverage with myIntegers array as param
        System.out.println("The average is " + getAverage(myIntegers));
    }
    //2.The function getIntegers output an array
    public static int[] getIntegers(int number) {
        //3.Invite user to enter n values
        System.out.println("Enter " + number + " integer values.\r");
        // 4. The array values is initialized with number slots available
        int[] values = new int[number];
        //5 Scanner invite user to enter value for 0 to the available number of slots
        for (int i=0; i< values.length; i++) {
            values[i] = scanner.nextInt();
        }
        //6. the function output the array values to the array myIntegers
        return values;
    }

    public static double getAverage(int[] array) {
        int sum = 0;
        for (int i = 0; i< array.length; i++) {
            sum += array[i];
        }

        return (double) sum/ (double)array.length;

    }
}
