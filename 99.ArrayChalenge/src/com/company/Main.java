package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("How many numbers in the array ?");
        int number = scanner.nextInt();
        int[] myArray = getIntegers(number);
        printArray(myArray);
        int[] sortedArray = sortIntegers(myArray);
        printArray(sortedArray);
    }

    public static int[] getIntegers(int number){
        Scanner scan = new Scanner(System.in);
        int[] getIntegers = new int[number];
        System.out.println("Enter " + number + " numbers");
        for (int i = 0; i< getIntegers.length; i++) {
            getIntegers[i] = scan.nextInt();
        }
        return getIntegers;
    }

    public static void printArray(int[] theArray) {
        for (int i = 0; i < theArray.length; i++){
            System.out.println(theArray[i]);
        }
    }

    public static int[] sortIntegers(int[] theArray) {
        //This bit of code creates a copy of theArray
//        int[] sortedArray = new int[theArray.length];
//        for(int i = 0; i< theArray.length; i++){
//            sortedArray[i] = theArray[i];
//        }
        //This next line of code does exactly the same using a method form java.utils.
        int[] sortedArray = Arrays.copyOf(theArray, theArray.length);
        boolean flag = true;
        int temp;
        while(flag) {
            flag = false;
            for(int i = 0; i < sortedArray.length - 1; i++){
                if (sortedArray[i] < sortedArray[i + 1]) {
                    temp = sortedArray[i];
                    sortedArray[i] = sortedArray[i+1];
                    sortedArray[i+1] = temp;
                    flag = true;
                }
            }
        }
        return sortedArray;
    }
}
