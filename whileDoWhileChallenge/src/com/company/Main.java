package com.company;

public class Main {

    public static void main(String[] args) {
        int startNumber, endNumber;
        startNumber = 2;
        endNumber = 20;
        while(startNumber <= endNumber){
            startNumber ++;
            if( !isEvenNumber(startNumber)){
                continue;
            }

            System.out.println(startNumber);
        }

    }

    public static boolean isEvenNumber(int number){
        return number % 2 == 0;
    }


}
